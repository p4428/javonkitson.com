### Need Help or Want to Report an Issue?

If you're experiencing difficulties with my app or have encountered a bug, I're here to help! Your feedback is invaluable in helping me improve. Please follow the steps below to contact me with details about the issue:

**1. Describe the Problem:**
- Clearly describe the issue you're facing. Include any error messages or unusual behavior you've noticed.

**2. Include a Screenshot:**
- Attach a screenshot of the issue. This will help me understand exactly what you're seeing. 

**3. Steps to Recreate:**
- Provide a step-by-step description of how to reproduce the problem. This information is crucial for me to identify and solve the issue efficiently.

**4. Device and App Information:**
- Include details about your device (model, OS version) and the version of my app that you are using.

**5. Send the Information:**
- Email this information to me at [javonkitson@gmail.com]. I aim to respond as quickly as possible.

**6. Additional Information:**
- If there are any other details or observations you think might be helpful, please include them.

Your support and understanding are greatly appreciated, and I'm committed to resolving your issues promptly. Thank you for using my app and for helping me make it better!